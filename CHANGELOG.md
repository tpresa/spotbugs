# spotbugs analyzer changelog

## v2.0.0
- Merge of the Maven, Gradle, Groovy and SBT analyzers with additional features:
  - Use the SpotBugs CLI for analysis.
  - Report correct source file paths.
  - Handle Maven multi module projects.
  - Only report vulnerabilities in source code files, ignore dependencies' libraries.
  - Add command line parameters and environment variable to set the paths of the Ant, Gradle, Maven, SBT and Java 
    executables.
  - Add the `--mavenCliOpts` command line parameter and `MAVEN_CLI_OPTS` environment to pass arguments to Maven.
